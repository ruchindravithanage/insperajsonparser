package parser;

import org.apache.log4j.Logger;
import parser.comparators.AssignmentAttributesComparatorApi;
import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.validators.AssignmentValidator;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Assignment Comparator can get the diff for a pair of Assignments
 * Assignment Comparator consists of
 *  1. validators : to check whether the given Assignments are valid to compute a diff
 *  2. assignment.comparators : actual comparison logic is embedded here
 * Assignment comparator loads the validators and assignment.comparators in the constructor using
 *  loadValidators and loadComparators methods
 */
public abstract class AssignmentComparator {

    private static final Logger LOGGER = Logger.getLogger(AssignmentComparator.class);
    private List<AssignmentAttributesComparatorApi> assignmentComparatorList = new ArrayList<AssignmentAttributesComparatorApi>();
    private List<AssignmentValidator> assignmentValidatorList = new ArrayList<AssignmentValidator>();

    /**
     * This method should be implemented to register any validator that is used
     * In the implementation please register all the validators needs to be used to check the eligibility
     * for the comparison for given pair of Assignments
     */
    protected abstract void loadValidators();

    /**
     * This method should be implemented to register any of the assignment.comparators used
     * Comparison logic will be executed according to the registered assignment.comparators in the implementation
     * The diff data is added to the Diff Container according to the order that assignment.comparators are called
     *
     * Eg : if user wants to get comparator1 diffs to shown before comparator2 diffs then the comparator 1 should be
     * registered before the comparator2
     */
    protected abstract void loadComparators();

    public AssignmentComparator(){
        this.loadValidators();
        this.loadComparators();
    }

    private List<AssignmentAttributesComparatorApi> getAssignmentComparatorList() {
        return assignmentComparatorList;
    }

    private List<AssignmentValidator> getAssignmentValidatorList() {
        return assignmentValidatorList;
    }

    public void registerValidator(AssignmentValidator assignmentValidator) {
        LOGGER.info(String.format("Validator Registered : %s", assignmentValidator.getClass()));
        this.getAssignmentValidatorList().add(assignmentValidator);
    }

    public void registerComparator(AssignmentAttributesComparatorApi assignementComparator) {
        LOGGER.info(String.format("Comparator Registered : %s", assignementComparator.getClass()));
        this.getAssignmentComparatorList().add(assignementComparator);
    }

    /**
     *
     * @param initialAssignment The initial Assignment Object
     * @param updatedAssignment The modified Assignment Object
     * @return Optional object of Assignment Difference Container ,
     * Optional is used because if any of the validators failed no diff will be returned
     */
    public Optional<AssignmentDifferenceContainer> getDiff(Assignment initialAssignment, Assignment updatedAssignment) {

        AssignmentDifferenceContainer assignmentDifferenceContainer = new AssignmentDifferenceContainer();

        //If any of the single validator is failed comparison will not be executed
        for (AssignmentValidator assignmentValidator : this.getAssignmentValidatorList()) {
            if (!assignmentValidator.validate(initialAssignment, updatedAssignment)) {
                LOGGER.debug("Validation failed no comparison will be executed");
                return Optional.empty();
            }
        }

        LOGGER.info(String.format("Diff Comparison Started on \n initial : [%s] \n modified : [%s]",
                initialAssignment.toString(), updatedAssignment.toString()));
        for (AssignmentAttributesComparatorApi assignmentComparatorApi : this.getAssignmentComparatorList()) {
            assignmentComparatorApi.compare(assignmentDifferenceContainer, initialAssignment, updatedAssignment);
        }

        return Optional.of(assignmentDifferenceContainer);
    }
}
