package parser;

import util.ComparatorID;

public class AssignmentComparatorFactory {

    public static AssignmentComparator getAssignmentComparator(ComparatorID comparatorID){
        switch (comparatorID){
            case DEFAULT:
                return new DefaultAssignmentComparator();
        }

        return null;
    }
}
