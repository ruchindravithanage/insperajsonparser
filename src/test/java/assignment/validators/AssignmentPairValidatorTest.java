package assignment.validators;

import parser.domainobjects.Assignment;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import parser.validators.AssignmentIdValidator;
import parser.validators.AssignmentValidator;

public class AssignmentPairValidatorTest {

    private Assignment initialAssignment;
    private Assignment modifiedAssignment;
    private AssignmentValidator assignmentValidator;

    @Before
    public void setup() {
        initialAssignment = new Assignment();
        modifiedAssignment = new Assignment();
    }

    @Test
    public void assignmentPairSameIdValidatorTest() {
        initialAssignment.setAssignmentId(10);
        modifiedAssignment.setAssignmentId(10);
        assignmentValidator = new AssignmentIdValidator();
        Assert.assertEquals(true, assignmentValidator.validate(initialAssignment, modifiedAssignment));
    }

    @Test
    public void assignmentPairDiffIdValidatorTest() {
        initialAssignment.setAssignmentId(10);
        modifiedAssignment.setAssignmentId(13);
        assignmentValidator = new AssignmentIdValidator();
        Assert.assertEquals(false, assignmentValidator.validate(initialAssignment, modifiedAssignment));
    }
}
