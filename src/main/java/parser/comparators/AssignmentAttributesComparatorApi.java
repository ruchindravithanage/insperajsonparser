package parser.comparators;

import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;

/**
 * Interface to implement a Comparator for a Pair of Assignments.
 * Assignment can consist of multiple attributes (variables)
 * this interface allows to compare any of the attributes in the given Assignment Pair and
 * add the difference to the Assignment Difference Container
 * The identified difference should be added to the Assignment Difference Container within the method implementation
 */
public interface AssignmentAttributesComparatorApi {

    /**
     * Developer can Assume that the given pair of Assignment Objects are valid to be compared
     * Those assignment validators are specified and registered seperately and will be executed before this comparisons
     *
     * @param assignmentDifferenceContainer Assignment Difference Container is the object that can store the
     *                                      diff of Assignment Attributes
     * @param initialAssignment the initial Assignment Status representation
     * @param updatedAssignment the changed/updated Assignment Status representation
     */
    void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                 Assignment initialAssignment,
                 Assignment updatedAssignment);
}
