package parser.domainobjects;

import net.sf.json.JSONObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import util.JsonConstants;
import util.JsonParseException;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Assignment Data Model
 *
 * Assignment consist of following components
 *  1. Unique ID
 *  2. Assignment Metadata
 *  3. Set of candidates
 *
 * Assignment object can be generated via a valid JSON object
 */

public class Assignment implements JsonObjectMapper {

    private static final Logger LOGGER = Logger.getLogger(Assignment.class);

    //Considered as a unique id for an Assignment
    private int assignmentId;

    private AssignmentMetaData assignmentMetaData;

    public Map<Integer, Candidate> assignmentCandidateContainer = new LinkedHashMap<>();

    public Assignment(){

    }

    /**
     * @param assignmentJson The Json object that can be used to set the Assignment attributes
     * @throws JsonParseException Exception when parsing the JSON values
     */
    public Assignment(JSONObject assignmentJson) throws JsonParseException {
        this.parse(assignmentJson);
    }

    /**
     * This method will get the respective values from the given input json object and set the attributes
     * @param jsonObject The Json object that can be used to set the Assignment attributes
     * @throws JsonParseException
     */
    @Override
    public void parse(JSONObject jsonObject) throws JsonParseException {
        try{
            this.assignmentId = jsonObject.getInt(JsonConstants.ASSIGNMENT_ID_JSON_IDENTIFIER);
            this.setAssignmentMetaData(new AssignmentMetaData());
            this.getAssignmentMetaData().parse(jsonObject.getJSONObject(JsonConstants.ASSIGNMENT_METADATA_JSON_IDENTIFIER));

            for (Object candidateJson : jsonObject.getJSONArray(JsonConstants.ASSIGNMENT_CANDIDATES_JSON_IDENTIFIER)) {
                Candidate candidate = new Candidate();
                candidate.parse((JSONObject) candidateJson);
                this.assignmentCandidateContainer.put(candidate.getCandidateId(), candidate);
            }
        }catch (Exception e){
            LOGGER.error("Cannot extract the values to populate the Assignment from the given Json Object");
            throw new JsonParseException(e);
        }

    }

    //TODO this method is not needed for the current requirements
    @Override
    public JSONObject toJSON() {
        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


    public AssignmentMetaData getAssignmentMetaData() {
        return assignmentMetaData;
    }

    public void setAssignmentMetaData(AssignmentMetaData assignmentMetaData) {
        this.assignmentMetaData = assignmentMetaData;
    }

    /**
     * @param candidate Candidate that needs to be added to the Assignment
     */
    public void addCandidate(Candidate candidate) {
        this.assignmentCandidateContainer.put(candidate.getCandidateId(), candidate);
    }

    /**
     *
     * @param candidateId the unique identifier for a candidate
     * @return the Candidate object matched for the given candidate id
     */
    public Candidate getCandidate(int candidateId) {
        return this.assignmentCandidateContainer.get(candidateId);
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }
}
