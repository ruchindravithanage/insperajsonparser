package parser.domainobjects;

import org.apache.commons.lang.ObjectUtils;
import util.MetaDataFieldName;

/**
 * A difference between a pair of Assignment MetaData fields can be stored
 */
public class AssignmentMetaDataFieldDiffIndicator<T> {

    //the metadata field name , this is used as the identifier
    private MetaDataFieldName metaDataFieldName;
    //The initial assignment meta data value for the respective field
    private T beforeValue;
    //The respective field value of the meta data after the modification of metadata
    private T afterValue;

    public AssignmentMetaDataFieldDiffIndicator(MetaDataFieldName metaDataFieldName, T beforeValue, T afterValue) {
        this.metaDataFieldName = metaDataFieldName;
        this.beforeValue = beforeValue;
        this.afterValue = afterValue;
    }

    public MetaDataFieldName getMetaDataFieldName() {
        return metaDataFieldName;
    }

    public String getMetaDataFieldDisplayName() {
        return metaDataFieldName.getDisplayValue();
    }

    public T getBeforeValue() {
        return beforeValue;
    }

    public T getAfterValue() {
        return afterValue;
    }

    public void setMetaDataFieldName(MetaDataFieldName metaDataFieldName) {
        this.metaDataFieldName = metaDataFieldName;
    }

    public void setBeforeValue(T beforeValue) {
        this.beforeValue = beforeValue;
    }

    public void setAfterValue(T afterValue) {
        this.afterValue = afterValue;
    }

    @Override
    public String toString() {
        return ObjectUtils.identityToString(this);
    }
}
