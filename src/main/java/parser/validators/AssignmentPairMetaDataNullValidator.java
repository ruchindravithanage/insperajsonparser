package parser.validators;

import org.apache.log4j.Logger;
import parser.domainobjects.Assignment;

/**
 * Validate whether the meta data fields on a given Assignment Pair is valid (not null)
 * Validates the
 *  1.Assignment Meta Data Object
 *  2.Assignment Meta Data Title field
 *  3.Assignment Meta Data Start Time field
 *  4.Assignment Meta Data End Time field
 */
public class AssignmentPairMetaDataNullValidator implements AssignmentValidator {

    private static final Logger LOGGER = Logger.getLogger(AssignmentPairMetaDataNullValidator.class);

    @Override
    public boolean validate(Assignment assignmentObj1, Assignment assignmentObj2) {

        if (assignmentObj1.getAssignmentMetaData() == null || assignmentObj2.getAssignmentMetaData() == null) {
            LOGGER.info("Assignment Metadata is Null , please specify meta data in both Assignments");
            return false;
        }

        if (assignmentObj1.getAssignmentMetaData().getTitle() == null || assignmentObj2.getAssignmentMetaData().getTitle() == null) {
            LOGGER.info("Assignment Metadata Title is Null , please specify meta data title in both Assignments");
            return false;
        }

        if (assignmentObj1.getAssignmentMetaData().getAssignmentStartTime() == null ||
                assignmentObj2.getAssignmentMetaData().getAssignmentStartTime()==null) {
            LOGGER.info("Assignment Metadata Start time is Null , please specify meta data Start time in both Assignments");
            return false;
        }

        if (assignmentObj1.getAssignmentMetaData().getAssignmentEndTime() == null ||
                assignmentObj2.getAssignmentMetaData().getAssignmentEndTime()==null) {
            LOGGER.info("Assignment Metadata End time is Null , please specify End time meta data in both Assignments");
            return false;
        }

        LOGGER.info("Assignment Meta Data in both Assignments are not null");
        return true;
    }
}
