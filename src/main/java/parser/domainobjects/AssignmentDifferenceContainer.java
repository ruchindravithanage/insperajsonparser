package parser.domainobjects;

import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import util.AppConstant;
import util.CandidateDiffCategory;
import util.JsonConstants;
import util.MetaDataFieldName;

import java.time.Instant;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Assignment Difference Container is implemented to store the difference in a pair of Assignment Objects
 * There is no change even any Domain object is modified
 * <p>
 * This can output a JSON there fore toJson is implemented according to the requirement
 */
public class AssignmentDifferenceContainer implements JsonObjectMapper {

    private static final Logger LOGGER = Logger.getLogger(AssignmentDifferenceContainer.class);

    //The modification details for assignment meta data fields
    private Map<MetaDataFieldName, AssignmentMetaDataFieldDiffIndicator> metaDataDiffContainer = new LinkedHashMap<>();

    /**
     * Key : Assignment Candidate Diff Category - the ENUM to identify the diff
     * Value : The list of candidates which indicates the Category
     * <p>
     * Linked Hash Map is used because the insertion order should be preserved
     */
    protected Map<CandidateDiffCategory, List<Candidate>> candidateDiffContainer = new LinkedHashMap<>();

    /**
     * @param candidateDiffCategory Diff category name
     * @param candidates            list of candidates to be added per the above diff category
     *                              if this is null or empty Diff category will not be added as a Diff
     */
    public void addCandidateDiff(CandidateDiffCategory candidateDiffCategory, List<Candidate> candidates) {
        if (candidates == null || candidates.isEmpty()) {
            LOGGER.info("Empty Candidate List received to be added as a Diff : Category " + candidateDiffCategory.getDisplayValue());
            LOGGER.info("Empty Candidate will be ignored");
        } else {
            this.candidateDiffContainer.put(candidateDiffCategory, candidates);
        }
    }

    public void addMetaDataDiff(AssignmentMetaDataFieldDiffIndicator assignmentMetaDataFieldDiffContainer) {
        this.metaDataDiffContainer.put(assignmentMetaDataFieldDiffContainer.getMetaDataFieldName(), assignmentMetaDataFieldDiffContainer);
    }

    /**
     * @param candidateDiffCategory the identifier to look up for diff in the candidates container
     * @return an empty list if category is not present in the diff container , otherwise respective
     * candidate list will be return by a map lookup
     */
    public List<Candidate> getCandidateDiff(CandidateDiffCategory candidateDiffCategory) {
        if (!candidateDiffContainer.containsKey(candidateDiffCategory)) {
            LOGGER.warn(candidateDiffCategory.getDisplayValue() + " does not contain any Candidates");
            return new ArrayList<>();
        }
        return candidateDiffContainer.get(candidateDiffCategory);
    }

    /**
     * @return the list of Meta Data Field Difference in Assignment pair storing object
     */
    public AssignmentMetaDataFieldDiffIndicator getMetaDataDiff(MetaDataFieldName metaDataFieldName) {
        return metaDataDiffContainer.get(metaDataFieldName);
    }


    @Override
    public void parse(JSONObject jsonObject) {
    }

    // this toJSON method can be changed and implement according to the diff JSON structure
    @Override
    public JSONObject toJSON() {

        JSONObject jsonRootObject = new JSONObject();

        //Set the assignment meta data difference according the metadata fields
        jsonRootObject.put(JsonConstants.ASSIGNMENT_METADATA_JSON_IDENTIFIER,
                //Creates a new List of Json objects based on the Assignment Meta data diff list
                this.metaDataDiffContainer.values().stream().sequential().map(assignmentMetaDataFieldDiffIndicator -> {
                    JSONObject obj = new JSONObject();
                    //Assignment Metadata field name
                    obj.put(JsonConstants.ASSIGNMENT_METADATA_DIFF_INDICATOR_FIELD_JSON_IDENTIFIER
                            , assignmentMetaDataFieldDiffIndicator.getMetaDataFieldName().getDisplayValue());
                    //Assignment Metadata before value and after value to json conversion

                    //if the object field is a time field then the respective time formatting will be applied
                    if (assignmentMetaDataFieldDiffIndicator.getBeforeValue() instanceof Instant) {
                        LOGGER.info("Assignment Meta Data Time Field converting to JSON : "
                                + assignmentMetaDataFieldDiffIndicator.getMetaDataFieldName().getDisplayValue());
                        LOGGER.info("Assignment Date Time Format : " + AppConstant.DIFF_DATE_FORMAT_STRING);
                        LOGGER.info("Assignment Date Time Zone Offset : " + AppConstant.TIME_ZONE_OFFSET);


                        obj.put(JsonConstants.ASSIGNMENT_METADATA_DIFF_INDICATOR_BEFORE_VALUE_JSON_IDENTIFIER
                                , ((Instant) assignmentMetaDataFieldDiffIndicator.getBeforeValue())
                                        .atOffset(ZoneOffset.ofHours(AppConstant.TIME_ZONE_OFFSET))
                                        .format(AppConstant.DIFF_DATE_FORMATTER)
                        );
                        obj.put(JsonConstants.ASSIGNMENT_METADATA_DIFF_INDICATOR_AFTER_VALUE_JSON_IDENTIFIER
                                , ((Instant) assignmentMetaDataFieldDiffIndicator.getAfterValue())
                                        .atOffset(ZoneOffset.ofHours(AppConstant.TIME_ZONE_OFFSET))
                                        .format(AppConstant.DIFF_DATE_FORMATTER));
                    } else {
                        obj.put(JsonConstants.ASSIGNMENT_METADATA_DIFF_INDICATOR_BEFORE_VALUE_JSON_IDENTIFIER
                                , assignmentMetaDataFieldDiffIndicator.getBeforeValue());
                        obj.put(JsonConstants.ASSIGNMENT_METADATA_DIFF_INDICATOR_AFTER_VALUE_JSON_IDENTIFIER
                                , assignmentMetaDataFieldDiffIndicator.getAfterValue());
                    }

                    return obj;
                }).collect(Collectors.toList()));

        //The current candidate diff map consists of candidate objects but as per requirement only the id is needed when
        //generating the diff json there for a new map is created
        LinkedHashMap<String, List<JSONObject>> tr = this.candidateDiffContainer.entrySet().stream().sequential().collect
                (Collectors.toMap(
                        //Map key is changed to be the display name of the diff category
                        entry -> entry.getKey().getDisplayValue(),
                        //Map value is changed to be the candidate id instead of Candidate object
                        entry -> entry.getValue().stream().map(candidate -> {
                            JSONObject obj = new JSONObject();
                            obj.put(JsonConstants.ASSIGNMENT_CANDIDATES_ID_JSON_IDENTIFIER, candidate.getCandidateId());
                            return obj;
                        }).collect(Collectors.toList()),
                        //candidate id should be unique there for no key merger is needed
                        (e1, e2) -> e1,
                        LinkedHashMap::new
                ));

        jsonRootObject.put(JsonConstants.ASSIGNMENT_CANDIDATES_JSON_IDENTIFIER, tr);
        return jsonRootObject;
    }
}
