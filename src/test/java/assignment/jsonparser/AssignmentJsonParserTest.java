package assignment.jsonparser;

import parser.domainobjects.Assignment;
import parser.domainobjects.Candidate;
import net.sf.json.JSONObject;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import util.JsonParseException;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.time.Instant;
import java.util.Calendar;
import java.util.Map;
import java.util.TimeZone;

public class AssignmentJsonParserTest {

    private static Logger LOGGER = Logger.getLogger(AssignmentJsonParserTest.class);

    @Test(expected = JsonParseException.class)
    public void invalidTimeFormatInAssignementMetaDataJsonParseTest() throws IOException, JsonParseException {
        String invalidJson = IOUtils.toString(this.getClass().getResourceAsStream("invalidMetaDataTimeAssignment.json"), "UTF-8");
        JSONObject assignmentJson = JSONObject.fromObject(invalidJson);
        LOGGER.info(ToStringBuilder.reflectionToString(assignmentJson).replace("{", "{\n"));
        Assignment assignment = new Assignment(assignmentJson);
    }

    @Test(expected = JsonParseException.class)
    public void nullAssignmentJsonParseTest() throws JsonParseException {
        Assignment assignment = new Assignment(null);
    }

    @Test
    public void validAssignmentJsonParsingOnAssignmentObjectTest() throws JsonParseException, ParseException, IOException {

        String validAssignmentJson = IOUtils.toString(this.getClass().getResourceAsStream("validAssignment.json"), "UTF-8");

        Map<Integer, Candidate> candidateTestDataMap = new HashedMap();
        candidateTestDataMap.put(10, new Candidate(10, "C1", 0));
        candidateTestDataMap.put(11, new Candidate(11, "C2", 10));
        candidateTestDataMap.put(12, new Candidate(12, "C3", 20));

        Assignment assignment = new Assignment();
        JSONObject assignmentJson = JSONObject.fromObject(validAssignmentJson);
        assignment.parse(assignmentJson);

        //Assertions
        Assert.assertNotNull(assignment.getAssignmentMetaData());
        Assert.assertEquals("Title", assignment.getAssignmentMetaData().getTitle());
        this.assertDate(assignment.getAssignmentMetaData().getAssignmentStartTime(), 2016, 1, 20, 10, 0, 0);
        this.assertDate(assignment.getAssignmentMetaData().getAssignmentEndTime(), 2016, 1, 20, 16, 0, 0);

        assignment.assignmentCandidateContainer.keySet().forEach(candidateId -> {
            this.assertCandidate(assignment.assignmentCandidateContainer.get(candidateId), candidateTestDataMap.get(candidateId));
        });

    }

    private void assertDate(Instant dateObj, int year, int month, int date, int hr, int min, int sec) {
        Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
        calendar.setTime(Date.from(dateObj));
        Assert.assertEquals("Year Assertion in Date Object", year, calendar.get(Calendar.YEAR));
        Assert.assertEquals("Month Assertion in Date Object", month - 1, calendar.get(Calendar.MONTH));
        Assert.assertEquals("Date Assertion in Date Object", date, calendar.get(Calendar.DATE));
        Assert.assertEquals("Hour Assertion in Date Object", hr, calendar.get(Calendar.HOUR_OF_DAY));
        Assert.assertEquals("Minute Assertion in Date Object", min, calendar.get(Calendar.MINUTE));
        Assert.assertEquals("Seconds Assertion in Date Object", sec, calendar.get(Calendar.SECOND));
    }

    private void assertCandidate(Candidate actualCandidate, Candidate expectedCandidate) {
        assertCandidate(
                actualCandidate,
                expectedCandidate.getCandidateId(),
                expectedCandidate.getCandidateName(),
                expectedCandidate.getExtraTime());
    }

    private void assertCandidate(Candidate candidate, int id, String candidateName, long extraTime) {
        Assert.assertEquals("Candidate Id check", id, candidate.getCandidateId());
        Assert.assertEquals("Candidate Name check", candidateName, candidate.getCandidateName());
        Assert.assertEquals("Candidate Extra Time check", extraTime, candidate.getExtraTime());
    }
}
