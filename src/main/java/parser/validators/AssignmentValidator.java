package parser.validators;

import parser.domainobjects.Assignment;

public interface AssignmentValidator {

    boolean validate(Assignment assignmentObj1, Assignment assignmentObj2);
}
