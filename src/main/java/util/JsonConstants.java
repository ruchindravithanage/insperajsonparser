package util;

/**
 * Json tag names
 */
public class JsonConstants {

    public static final String ASSIGNMENT_ID_JSON_IDENTIFIER = "id";
    public static final String ASSIGNMENT_METADATA_JSON_IDENTIFIER = "meta";
    public static final String ASSIGNMENT_METADATA_TITLE_JSON_IDENTIFIER = "title";
    public static final String ASSIGNMENT_METADATA_START_TIME_JSON_IDENTIFIER = "startTime";
    public static final String ASSIGNMENT_METADATA_END_TIME_JSON_IDENTIFIER = "endTime";
    public static final String ASSIGNMENT_CANDIDATES_JSON_IDENTIFIER = "candidates";
    public static final String ASSIGNMENT_CANDIDATES_ID_JSON_IDENTIFIER = "id";
    public static final String ASSIGNMENT_CANDIDATES_CANDIDATE_NAME_JSON_IDENTIFIER = "candidateName";
    public static final String ASSIGNMENT_CANDIDATES_EXTRA_TIME_JSON_IDENTIFIER = "extraTime";
    public static final String ASSIGNMENT_METADATA_DIFF_INDICATOR_FIELD_JSON_IDENTIFIER = "field";
    public static final String ASSIGNMENT_METADATA_DIFF_INDICATOR_BEFORE_VALUE_JSON_IDENTIFIER = "before";
    public static final String ASSIGNMENT_METADATA_DIFF_INDICATOR_AFTER_VALUE_JSON_IDENTIFIER = "after";

}
