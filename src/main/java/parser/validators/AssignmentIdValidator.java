package parser.validators;

import parser.domainobjects.Assignment;

public class AssignmentIdValidator implements AssignmentValidator {
    @Override
    public boolean validate(Assignment assignmentObj1, Assignment assignmentObj2) {
        if(assignmentObj1.getAssignmentId() == assignmentObj2.getAssignmentId()){
            return true;
        }
        return false;
    }
}
