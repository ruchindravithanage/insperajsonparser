package assignment.tojson;

import parser.domainobjects.AssignmentDifferenceContainer;
import net.sf.json.JSONObject;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;

public class AssignmentDiffJsonConverter {

    private static Logger LOGGER = Logger.getLogger(AssignmentDiffJsonConverter.class);
    private AssignmentDifferenceContainer assignmentDifferenceContainer;

    @Test
    public void convertAssignmentPairDiffToValidJson() throws IOException {
        assignmentDifferenceContainer = new AssignmentDifferenceContainer();
        String emptyJson = IOUtils.toString(this.getClass().getResourceAsStream("emptydiff.json"), "UTF-8");
        JSONObject emptyDiffJson = JSONObject.fromObject(emptyJson);
        JSONObject diffJson = assignmentDifferenceContainer.toJSON();
        LOGGER.info("Converted Assignment Diff Container :  "+diffJson);
        Assert.assertEquals("Empty Diff", emptyDiffJson, diffJson);
    }
}
