package parser.comparators;

import org.apache.log4j.Logger;
import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.domainobjects.AssignmentMetaDataFieldDiffIndicator;
import util.MetaDataFieldName;

/**
 * This class will check the Title field in the Assignment Meta Data
 * If title is different in given Assignment pair of objects a meta data difference will generate using
 * given assignment objects and added to the Assignment Difference Data Container
 */
public class EditedAssignmentMetaDataTitleFieldComparator implements AssignmentAttributesComparatorApi {

    private static final Logger LOGGER = Logger.getLogger(EditedAssignmentMetaDataTitleFieldComparator.class);

    @Override
    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        //Compare the title field value from the Assignment Meta Data
        if (!initialAssignment.getAssignmentMetaData().getTitle().equalsIgnoreCase(
                updatedAssignment.getAssignmentMetaData().getTitle())) {

            AssignmentMetaDataFieldDiffIndicator assignmentMetaDataFieldDiffIndicator =
                    new AssignmentMetaDataFieldDiffIndicator<>(
                            MetaDataFieldName.ASSIGNMENT_META_DATA_TITLE_FIELD,
                            initialAssignment.getAssignmentMetaData().getTitle(),
                            updatedAssignment.getAssignmentMetaData().getTitle()
                    );

            LOGGER.debug("Assignment Meta Data Title field is not Equal");
            LOGGER.debug(String.format("Assignment Meta Data Field Difference : %s", assignmentMetaDataFieldDiffIndicator.toString()));
            assignmentDifferenceContainer.addMetaDataDiff(assignmentMetaDataFieldDiffIndicator);
        }

    }
}
