package util;

/**
 * This exception indicates any of the exceptions occurred when Json is parsed to update object fields
 */
public class JsonParseException extends Exception {

    public JsonParseException(String errorMessage) {
        super(errorMessage);
    }

    public JsonParseException(Exception e) {
        super(e);
    }

}
