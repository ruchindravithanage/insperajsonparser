package parser.domainobjects;

import net.sf.json.JSONObject;
import util.JsonParseException;

public interface JsonObjectMapper {

    void parse(JSONObject jsonObject) throws JsonParseException;

    JSONObject toJSON();
}
