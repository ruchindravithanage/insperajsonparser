import net.sf.json.JSONObject;
import org.apache.log4j.Logger;
import parser.AssignmentComparator;
import parser.AssignmentComparatorFactory;
import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import util.ComparatorID;
import util.JsonParseException;

/**
 * This parser will take pair of JSON objects representing assignments and return the difference of the objects
 * as a JSON object
 */
public class Parser {

    private static final Logger LOGGER = Logger.getLogger(Parser.class);

    /**
     * Compare two json objects that describes the state of a Assignment and return the difference according to the
     * following format
     *
     * @param before Json object that contains the data of the Assignment at the beginning
     * @param after  Json object that contains the data of the Assignment after modifying
     * @return Json object that describes the difference of the given two assignments
     * @throws JsonParseException any error occurred while parsing the input json will capture with this exception
     */
    public JSONObject parse(JSONObject before, JSONObject after) throws JsonParseException {
        return this.parse(AssignmentComparatorFactory
                .getAssignmentComparator(ComparatorID.DEFAULT), before, after);
    }

    /**
     * Compare two json objects that describes the state of a Assignment and return the difference according to the
     * following format
     *
     * @param assignmentComparator The comparator that is used to get the diff of given assignment pair
     *                             Comparator is a set of assignment pair validators and set of compare logic
     * @param before Json object that contains the data of the Assignment at the beginning
     * @param after  Json object that contains the data of the Assignment after modifying
     * @return Json object that describes the difference of the given two assignments
     * @throws JsonParseException any error occurred while parsing the input json will capture with this exception
     */
    public JSONObject parse(AssignmentComparator assignmentComparator, JSONObject before, JSONObject after) throws JsonParseException {

        Assignment initialAssignment = new Assignment(before);
        Assignment updatedAssignment = new Assignment(after);
        AssignmentDifferenceContainer assignmentDifferenceContainer = assignmentComparator
                .getDiff(initialAssignment, updatedAssignment).get();

        return assignmentDifferenceContainer.toJSON();
    }

}
