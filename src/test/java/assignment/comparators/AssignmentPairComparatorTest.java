package assignment.comparators;

import net.sf.json.JSONObject;
import org.apache.commons.io.IOUtils;
import parser.AssignmentComparator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import parser.comparators.*;
import parser.domainobjects.*;
import util.CandidateDiffCategory;
import util.MetaDataFieldName;

import java.io.IOException;
import java.time.Instant;
import java.util.Optional;

public class AssignmentPairComparatorTest {

    Assignment initialAssignment;
    Assignment modifiedAssignment;

    @Before
    public void setup() {

        initialAssignment = new Assignment();
        initialAssignment.addCandidate(new Candidate(1, "TestCandidate_1", 1));
        initialAssignment.addCandidate(new Candidate(2, "TestCandidate_2", 2));
        initialAssignment.addCandidate(new Candidate(3, "TestCandidate_3", 3));
        initialAssignment.addCandidate(new Candidate(1000, "TestCandidate_1000", 1000));
        initialAssignment.addCandidate(new Candidate(1001, "TestCandidate_1001", 1001));
        initialAssignment.addCandidate(new Candidate(1002, "TestCandidate_1002", 1002));

        modifiedAssignment = new Assignment();

    }

    @Test
    public void deletedCandidatesInAssignmentPairComparatorTest() {

        AssignmentComparator assignmentComparator = new AssignmentComparator() {

            @Override
            protected void loadValidators() {
            }

            @Override
            protected void loadComparators() {
                registerComparator(new DeletedCandidatesComparator());
            }
        };

        modifiedAssignment.addCandidate(new Candidate(1, "TestCandidate_1", 1));
        modifiedAssignment.addCandidate(new Candidate(3, "TestCandidate_3", 3));

        Optional<AssignmentDifferenceContainer> diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Deleted Candidate Count on Test Data set is 4", 4, diff.get().getCandidateDiff(CandidateDiffCategory.REMOVED).size());

    }

    @Test
    public void addedCandidatesInAssignmentPairComparatorTest() {

        AssignmentComparator assignmentComparator = new AssignmentComparator() {
            @Override
            protected void loadValidators() {

            }

            @Override
            protected void loadComparators() {
                registerComparator(new NewlyAddedCandidateComparator());
            }
        };

        modifiedAssignment.addCandidate(new Candidate(1, "TestCandidate_1", 1));
        modifiedAssignment.addCandidate(new Candidate(3, "TestCandidate_3", 3));

        Optional<AssignmentDifferenceContainer> diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 0", 0,
                diff.get().getCandidateDiff(CandidateDiffCategory.ADDED).size());

        //Added a new candidate to the modified assignment
        modifiedAssignment.addCandidate(new Candidate(4, "TestCandidate_2", 19));
        diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 1", 1,
                diff.get().getCandidateDiff(CandidateDiffCategory.ADDED).size());

        //Added 3 new students to the modified assignment
        modifiedAssignment.addCandidate(new Candidate(5, "TestCandidate_2", 19));
        modifiedAssignment.addCandidate(new Candidate(6, "TestCandidate_2", 19));
        modifiedAssignment.addCandidate(new Candidate(7, "TestCandidate_2", 19));
        diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 4", 4,
                diff.get().getCandidateDiff(CandidateDiffCategory.ADDED).size());
    }

    @Test
    public void editedCandidatesInAssignmentPairComparatorTest() {

        AssignmentComparator assignmentComparator = new AssignmentComparator() {
            @Override
            protected void loadValidators() {

            }

            @Override
            protected void loadComparators() {
                registerComparator(new EditedCandidatesComparator());
            }
        };

        Optional<AssignmentDifferenceContainer> diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 0", 0,
                diff.get().getCandidateDiff(CandidateDiffCategory.EDITED).size());

        //Added a new candidate to the modified assignment
        modifiedAssignment.addCandidate(new Candidate(1000, "TestCandidate_1000_mod", 19));
        diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 1", 1,
                diff.get().getCandidateDiff(CandidateDiffCategory.EDITED).size());

        //Added 3 new students to the modified assignment
        modifiedAssignment.addCandidate(new Candidate(1001, "TestCandidate_1001_mod", 1001));
        modifiedAssignment.addCandidate(new Candidate(1002, "TestCandidate_1002_mod", 1003));
        diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        Assert.assertEquals("Newly Added Candidate count on Test Data set is 3", 3,
                diff.get().getCandidateDiff(CandidateDiffCategory.EDITED).size());
    }

    @Test
    public void titleMetaDataFieldAssignmentPairComparatorTest() {

        AssignmentComparator assignmentComparator = new AssignmentComparator() {
            @Override
            protected void loadValidators() {

            }

            @Override
            protected void loadComparators() {
                registerComparator(new EditedAssignmentMetaDataTitleFieldComparator());
            }
        };

        initialAssignment.setAssignmentMetaData(new AssignmentMetaData("Initial Assignment",
                Instant.parse("2019-02-09T10:15:30.00Z"), Instant.parse("2019-02-19T14:15:36.00Z")));
        modifiedAssignment.setAssignmentMetaData(new AssignmentMetaData("Modified Assignment",
                Instant.parse("2019-02-19T10:15:30.00Z"), Instant.parse("2019-05-19T14:15:36.00Z")));

        Optional<AssignmentDifferenceContainer> diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        AssignmentMetaDataFieldDiffIndicator assignmentMetaDataFieldDiffIndicator =
                diff.get().getMetaDataDiff(MetaDataFieldName.ASSIGNMENT_META_DATA_TITLE_FIELD);
        Assert.assertEquals("Title Before Value", "Initial Assignment", assignmentMetaDataFieldDiffIndicator.getBeforeValue());
        Assert.assertEquals("Title After Value", "Modified Assignment", assignmentMetaDataFieldDiffIndicator.getAfterValue());
    }

    @Test
    public void startTimeEndTimeMetaDataFieldAssignmentPairComparatorTest() throws IOException {

        AssignmentComparator assignmentComparator = new AssignmentComparator() {
            @Override
            protected void loadValidators() {

            }

            @Override
            protected void loadComparators() {
                registerComparator(new EditedAssignmentMetaDataTimeFieldComparator());
            }
        };

        initialAssignment.setAssignmentMetaData(new AssignmentMetaData("Initial Assignment",
                Instant.parse("2019-02-09T10:15:30.00Z"), Instant.parse("2019-02-19T14:15:36.00Z")));
        modifiedAssignment.setAssignmentMetaData(new AssignmentMetaData("Modified Assignment",
                Instant.parse("2019-02-19T13:15:30.00Z"), Instant.parse("2019-02-19T14:19:36.00Z")));

        Optional<AssignmentDifferenceContainer> diff = assignmentComparator.getDiff(initialAssignment, modifiedAssignment);
        AssignmentMetaDataFieldDiffIndicator assignmentStartTimeMetaDataFieldDiffIndicator =
                diff.orElse(new AssignmentDifferenceContainer()).getMetaDataDiff(MetaDataFieldName.ASSIGNMENT_META_DATA_START_TIME_FIELD);
        Assert.assertEquals("Start Time Before Value", Instant.parse("2019-02-09T10:15:30Z"),
                assignmentStartTimeMetaDataFieldDiffIndicator.getBeforeValue());
        Assert.assertEquals("Start Time After Value", Instant.parse("2019-02-19T13:15:30Z"),
                assignmentStartTimeMetaDataFieldDiffIndicator.getAfterValue());

        AssignmentMetaDataFieldDiffIndicator assignmentEndTimeMetaDataFieldDiffIndicator =
                diff.orElse(new AssignmentDifferenceContainer()).getMetaDataDiff(MetaDataFieldName.ASSIGNMENT_META_DATA_End_TIME_FIELD);
        Assert.assertEquals("End Time Before Value", Instant.parse("2019-02-19T14:15:36Z"),
                assignmentEndTimeMetaDataFieldDiffIndicator.getBeforeValue());
        Assert.assertEquals("End Time After Value", Instant.parse("2019-02-19T14:19:36Z"),
                assignmentEndTimeMetaDataFieldDiffIndicator.getAfterValue());

        String diffJsonString = IOUtils.toString(this.getClass().getResourceAsStream("timeFieldMetaData.json"), "UTF-8");

        Assert.assertEquals("Time Meta Data Diff Json", JSONObject.fromObject(diffJsonString),diff.get().toJSON());
    }

}
