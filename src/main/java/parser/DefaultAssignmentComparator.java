package parser;

import parser.comparators.*;
import parser.validators.AssignmentIdValidator;
import parser.validators.AssignmentPairMetaDataNullValidator;

/**
 * Default Comparison Profile
 *
 * Validator is added to check whether the ids are equal for the given pair of assignments before comparison
 *
 * This is the current requirement as the comparator , this will
 * compare and look for
 *  1. Changes in Title Field in Assignment Meta Data
 *  2. Changes in Start Time and End Time Fields in Assignment Meta Data (Implemented as a single comparator)
 *  3. Edited Candidates
 *  4. Newly Added candidates
 *  5. Deleted Candidates
 *
 * for the given pair of Assignment Objects
 */
public class DefaultAssignmentComparator extends AssignmentComparator {

    public void loadComparators(){
        this.registerComparator(new EditedAssignmentMetaDataTitleFieldComparator());
        this.registerComparator(new EditedAssignmentMetaDataTimeFieldComparator());
        this.registerComparator(new EditedCandidatesComparator());
        this.registerComparator(new NewlyAddedCandidateComparator());
        this.registerComparator(new DeletedCandidatesComparator());
    }

    public void loadValidators(){
        this.registerValidator(new AssignmentIdValidator());
        this.registerValidator(new AssignmentPairMetaDataNullValidator());
    }
}
