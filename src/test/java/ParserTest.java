import parser.AssignmentComparatorFactory;
import net.sf.json.JSONObject;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.ComparatorID;
import util.JsonParseException;

import java.io.IOException;

/**
 *
 */
public class ParserTest {

    private Parser parser = new Parser();
    private String beforeContent;
    private String afterContent;
    private String diffContent;


    @Before
    public void setup() throws IOException {
        beforeContent = IOUtils.toString(
                this.getClass().getResourceAsStream("before.json"),
                "UTF-8"
        );
        afterContent = IOUtils.toString(
                this.getClass().getResourceAsStream("after.json"),
                "UTF-8"
        );
        diffContent = IOUtils.toString(
                this.getClass().getResourceAsStream("diff.json"),
                "UTF-8"
        );
    }

    @Test
    public void validAssignmentJsonPairDiffCheckTest() throws JsonParseException {
        Assert.assertEquals(
                JSONObject.fromObject(diffContent),
                parser.parse(JSONObject.fromObject(beforeContent), JSONObject.fromObject(afterContent)));

        Assert.assertEquals(
                JSONObject.fromObject(diffContent),
                parser.parse(AssignmentComparatorFactory.getAssignmentComparator(ComparatorID.DEFAULT),
                        JSONObject.fromObject(beforeContent), JSONObject.fromObject(afterContent)));
    }

    @Test
    public void validAssignmentJsonPairDiffCheckWithGivenComparatorTest() throws JsonParseException {
        Assert.assertEquals(
                JSONObject.fromObject(diffContent),
                parser.parse(AssignmentComparatorFactory.getAssignmentComparator(ComparatorID.DEFAULT),
                        JSONObject.fromObject(beforeContent), JSONObject.fromObject(afterContent)));
    }



}
