package parser.comparators;

import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.domainobjects.Candidate;
import util.CandidateDiffCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * <H2>
 * Implementation of Assignment Attribute Comparator API
 * </H2>
 *
 * Assignment has a set of candidates
 * This comparator will check the edited candidates in the updated Assignment object contrast to the
 * initial Assignment object
 * <p>
 * Filter applied as : If any candidate present in the initial assignment is present
 * in the updated assignment and that candidate attributes are not equal then that candidate
 * is identified as a edited candidate.
 */
public class EditedCandidatesComparator implements AssignmentAttributesComparatorApi {

    @Override
    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        List<Candidate> editedCandidates = new ArrayList<>();

        initialAssignment.assignmentCandidateContainer.keySet().forEach(candidateId -> {
            if (updatedAssignment.assignmentCandidateContainer.containsKey(candidateId)) {
                //Candidate has implemented the equals method to check the equality
                if (!initialAssignment.getCandidate(candidateId).equals(updatedAssignment.getCandidate(candidateId))) {
                    editedCandidates.add(initialAssignment.getCandidate(candidateId));
                }
            }
        });
        assignmentDifferenceContainer.addCandidateDiff(CandidateDiffCategory.EDITED, editedCandidates);
    }
}
