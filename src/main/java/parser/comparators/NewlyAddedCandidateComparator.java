package parser.comparators;

import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.domainobjects.Candidate;
import util.CandidateDiffCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * <H2>
 * Implementation of Assignment Attribute Comparator API
 * </H2>
 *
 * Assignment has a set of candidates
 * This comparator will check the newly added candidates in the updated Assignment object contrast to the
 * initial Assignment object
 * <p>
 * Filter applied as : If any candidate present in the updated assignment is not present
 * in the initial assignment then that candidate is identified as a new candidate.
 */
public class NewlyAddedCandidateComparator implements AssignmentAttributesComparatorApi {

    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        List<Candidate> newlyAddedCandidates = new ArrayList<>();
        updatedAssignment.assignmentCandidateContainer.keySet().forEach(candidateId -> {
            if (!initialAssignment.assignmentCandidateContainer.containsKey(candidateId)) {
                newlyAddedCandidates.add(updatedAssignment.getCandidate(candidateId));
            }
        });
        assignmentDifferenceContainer.addCandidateDiff(CandidateDiffCategory.ADDED, newlyAddedCandidates);
    }
}
