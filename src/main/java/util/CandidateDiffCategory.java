package util;

public enum CandidateDiffCategory {
    EDITED("edited"),ADDED("added"),REMOVED("removed");

    CandidateDiffCategory(String displayValue) {
        this.displayValue = displayValue;
    }

    //Name to be displayed at the output as the category name
    private String displayValue;

    public String getDisplayValue() {
        return displayValue;
    }
}
