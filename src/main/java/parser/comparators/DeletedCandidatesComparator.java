package parser.comparators;

import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.domainobjects.Candidate;
import util.CandidateDiffCategory;

import java.util.ArrayList;
import java.util.List;

/**
 * <H2>
 * Implementation of Assignment Attribute Comparator API
 * </H2>
 *
 * Assignment has a set of candidates
 * This comparator will check the deleted candidates in the updated Assignment object contrast to the
 * initial Assignment object
 * <p>
 * Filter applied as : If any candidate present in the initial assignment is not present
 * in the updated assignment that candidate is identified as a deleted candidate.
 */
public class DeletedCandidatesComparator implements AssignmentAttributesComparatorApi {

    @Override
    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        //Container to add deleted candidates
        List<Candidate> deletedCandidates = new ArrayList<>();

        initialAssignment.assignmentCandidateContainer.keySet().forEach(candidateId -> {
            if (!updatedAssignment.assignmentCandidateContainer.containsKey(candidateId)) {
                deletedCandidates.add(initialAssignment.getCandidate(candidateId));
            }
        });
        assignmentDifferenceContainer.addCandidateDiff(CandidateDiffCategory.REMOVED, deletedCandidates);
    }
}
