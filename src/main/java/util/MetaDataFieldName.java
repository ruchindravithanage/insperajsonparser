package util;

public enum MetaDataFieldName {

    ASSIGNMENT_META_DATA_TITLE_FIELD("title"),
    ASSIGNMENT_META_DATA_START_TIME_FIELD("starTime"),
    ASSIGNMENT_META_DATA_End_TIME_FIELD("endTime");

    MetaDataFieldName(String displayValue) {
        this.displayValue = displayValue;
    }

    //Name to be displayed at the output as the category name
    private String displayValue;

    public String getDisplayValue() {
        return displayValue;
    }
}
