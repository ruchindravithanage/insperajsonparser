package parser.domainobjects;

import net.sf.json.JSONObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import util.JsonConstants;
import util.JsonParseException;

import java.time.Instant;

/**
 * The Assignment Meta Data object is a part of an Assignment
 * The time fields are stored as Instant when there is a requirement of displaying it will be converted
 * <p>
 * Assignment Meta data can be modified by adding new fields (Variables)
 * The metadata fields are loaded via a json object therefore
 * parse method and equals method should be changed according to the added fields
 */
public class AssignmentMetaData implements JsonObjectMapper {

    private final static Logger LOGGER = Logger.getLogger(AssignmentMetaData.class);

    //Title of the Assignment
    private String title;

    //Assignment Started time
    private Instant assignmentStartTime;

    //Assignment Finished time
    private Instant assignmentEndTime;

    public AssignmentMetaData(String title, Instant assignmentStartTime, Instant assignmentEndTime) {
        this.title = title;
        this.assignmentStartTime = assignmentStartTime;
        this.assignmentEndTime = assignmentEndTime;
    }

    public AssignmentMetaData() {
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Instant getAssignmentStartTime() {
        return assignmentStartTime;
    }

    public void setAssignmentStartTime(Instant assignmentStartTime) {
        this.assignmentStartTime = assignmentStartTime;
    }

    public Instant getAssignmentEndTime() {
        return assignmentEndTime;
    }

    public void setAssignmentEndTime(Instant assignmentEndTime) {
        this.assignmentEndTime = assignmentEndTime;
    }

    public boolean equals(AssignmentMetaData assignmentMetaData) {
        if (!this.title.equalsIgnoreCase(assignmentMetaData.getTitle())
                || this.assignmentStartTime.compareTo(assignmentMetaData.getAssignmentStartTime()) != 0
                || this.assignmentEndTime.compareTo(assignmentMetaData.getAssignmentEndTime()) != 0
        ) {
            return false;
        }
        return true;
    }

    @Override
    public void parse(JSONObject jsonObject) throws JsonParseException {
        try {
            this.title = jsonObject.getString(JsonConstants.ASSIGNMENT_METADATA_TITLE_JSON_IDENTIFIER);
            this.assignmentStartTime = Instant.parse
                    (jsonObject.getString(JsonConstants.ASSIGNMENT_METADATA_START_TIME_JSON_IDENTIFIER));
            this.assignmentEndTime = Instant.parse
                    (jsonObject.getString(JsonConstants.ASSIGNMENT_METADATA_END_TIME_JSON_IDENTIFIER));
        } catch (Exception e) {
            LOGGER.error("Cannot extract the values to populate Assignment Meta Data from the given Json Object");
            throw new JsonParseException(e);
        }
    }

    //TODO this method is not needed for the current requirements
    @Override
    public JSONObject toJSON() {
        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
