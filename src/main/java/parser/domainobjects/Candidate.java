package parser.domainobjects;

import net.sf.json.JSONObject;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.log4j.Logger;
import util.JsonConstants;
import util.JsonParseException;

/**
 * Candidate data model
 * Assignment consists of set of candidates
 * <p>
 * Candidate can be modified by adding new fields (Variables)
 * The added candidate fields are loaded via a json object therefore
 * parse method and equals method should be changed according to the added fields
 */
public class Candidate implements JsonObjectMapper {

    private static final Logger LOGGER = Logger.getLogger(Candidate.class);

    //Unique Identifier of the candidate
    private int candidateId;

    private String candidateName;

    private long extraTime;

    public Candidate(int candidateId, String candidateName, long extraTime) {
        this.candidateId = candidateId;
        this.candidateName = candidateName;
        this.extraTime = extraTime;
    }

    public Candidate() {

    }

    public int getCandidateId() {
        return candidateId;
    }

    public void setCandidateId(int candidateId) {
        this.candidateId = candidateId;
    }

    public String getCandidateName() {
        return candidateName;
    }

    public void setCandidateName(String candidateName) {
        this.candidateName = candidateName;
    }

    public long getExtraTime() {
        return extraTime;
    }

    public void setExtraTime(long extraTime) {
        this.extraTime = extraTime;
    }

    public boolean equals(Candidate candidate) {
        if (this.candidateId != candidate.getCandidateId()
                || !this.candidateName.equalsIgnoreCase(candidate.getCandidateName())
                || this.extraTime != candidate.getExtraTime()) {
            return false;
        }
        return true;
    }

    @Override
    public void parse(JSONObject jsonObject) throws JsonParseException {
        try {
            this.candidateId = jsonObject.getInt(JsonConstants.ASSIGNMENT_CANDIDATES_ID_JSON_IDENTIFIER);
            this.candidateName = jsonObject.getString(JsonConstants.ASSIGNMENT_CANDIDATES_CANDIDATE_NAME_JSON_IDENTIFIER);
            this.extraTime = jsonObject.getLong(JsonConstants.ASSIGNMENT_CANDIDATES_EXTRA_TIME_JSON_IDENTIFIER);
        } catch (Exception e) {
            LOGGER.error("Json Parse error occurred while populating candidate attributes : " + jsonObject);
            throw new JsonParseException(e);
        }
    }

    @Override
    public JSONObject toJSON() {
        return null;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
