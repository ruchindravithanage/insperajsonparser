![](https://www.inspera.com/hubfs/InsperaFall2015/insperaassessment.svg)
2019-02-09 23:29:49 Saturday
[Inspera Json Parser BitBucket Url][Inspera Json Parser Bitbucket]

## Json Comparator

This is a parser that takes two JSONObjects which represent an Assignment (net.sf.json.JSONObject) as input and returns 
a JSONObject that details the changes between the two input objects according to a specific Json Schema.

Use the parser method in the Parser class to get the difference in given two JSONObjects 
representing assignments. There is a default compare logic embedded for generic usage which will contain the 
following difference in given two JSONObjects

- The added candidates
- The deleted candidates
- The modified candidates
- The metadata Title field
- The metadata StartTime field
- The metadata EndTime field

There is another method which can be used with a custom comparator and a validator.
Validator validates whether the given Assignments (JsonObjects) are eligible to 
perform the comparison logic. Comparator can take two Assignments and return 
the difference of two Assignments through a Assignment Difference Container. 
Assignment Difference Container consists a list of Assignment Meta Data Difference
indicating objects and a list of Candidates difference indicating objects along with
a difference indication category Id.
The difference in the given two JsonObjects are specified with 

- For time fields the timezone should be CEST (Oslo - UTC+2)
- Changes in the Meta Data Fields are indicating with before/after values
- Changes in the candidates are grouped 

#Design

#####Assumptions 

- Json Object describes an Assignment
- Assignment has a Id which is a unique identifier for an Assignment
- Only Assignments with same IDs can be compared
- Assignment has a Assignment metadata component and set of candidates
- Candidate Id is a unique identifier for an Candidate
- Diff Json Time Fields are shown using Time zone : UTC+2
- No validations are added to validate Assignment metadata
 
Start time and End time fields to check whether the End time is after the Start time.
But a simple validator can be added easily to validate this if there is a requirement


High level design diagram can be seen in the following diagram.

![High Level Design Diagram](https://bitbucket.org/ruchindravithanage/insperajsonparser/raw/a456a78f125b1d5de274c594af8af63aa6c67ab6/readme.images/Package%20parser.png)

## Setup
- Install java 8 
- Install Maven
- Run "mvn clean install" to download dependencies

Clone the project using `git clone git@bitbucket.org:ruchindravithanage/insperajsonparser.git`

#Developer Guide

The comparator can be changed and feed to the parser. Pleas use java instant 
to specify all the date time related modifications. 

- Output JSON date/time format can be configured
- If a new attribute is added to the Assignment only Domain objects 

(Assignment/Assignment meta data or Candidate) should be updated.
If the added attribute affects to the comparison logic please update them too.

##### Assignment Attribute Comparator API
Implement this API to develop a new logic to compare two assignment objects.
The difference should be added to the parsed assignment difference indicating object
by the developer inside the implementation.

```java
/**
 * Implementation of Assignment Attribute Comparator API
 *
 * Assignment has a set of candidates
 * This comparator will check the deleted candidates in the updated Assignment object contrast to the
 * initial Assignment object
 */
public class DeletedCandidatesComparator implements AssignmentAttributesComparatorApi {

    @Override
    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        //Container to add deleted candidates
        List<Candidate> deletedCandidates = new ArrayList<>();

        initialAssignment.assignmentCandidateContainer.keySet().forEach(candidateId -> {
            if (!updatedAssignment.assignmentCandidateContainer.containsKey(candidateId)) {
                deletedCandidates.add(initialAssignment.getCandidate(candidateId));
            }
        });
        //Computed Diff is Added to the diff indicating object
        assignmentDifferenceContainer.addCandidateDiff(CandidateDiffCategory.REMOVED, deletedCandidates);
    }
}
```

##### Assignment Validator API
Implement this API to develop a new validator logic to check the eligibility
of given Assignment objects to carry the comparison logic in the register comparators.
Return true or false based on the validation logic

```java
/**
 * Implementation of Assignment Validator API
 *
 * Assignment has an id attribute. This validator checks whether the
 * ids of the given two Assignments are same
 */
public class AssignmentIdValidator implements AssignmentValidator {
    @Override
    public boolean validate(Assignment assignmentObj1, Assignment assignmentObj2) {
        if(assignmentObj1.getAssignmentId() == assignmentObj2.getAssignmentId()){
            return true;
        }
        return false;
    }
}
```

#####Implement a new Assignment Comparator
Implement the AssignmentComparator abstract class and load the above implemented
validators and comparators using the implementation of following abstract methods.

- loadValidators() : Register the validators needs to be used
- loadComparators() : Register the comparators needs to be used

use registerComparator and registerValidator method in the super class to register
```java
/**
 * Default Comparison Profile
 *
 * Validator is added to check whether the ids are equal for the given pair of assignments before comparison
 *
 * This is the current requirement as the comparator , this will
 * compare and look for
 *  1. Changes in Title Field in Assignment Meta Data
 *  2. Changes in Start Time and End Time Fields in Assignment Meta Data (Implemented as a single comparator)
 *  3. Edited Candidates
 *  4. Newly Added candidates
 *  5. Deleted Candidates
 *
 * for the given pair of Assignment Objects
 */
public class DefaultAssignmentComparator extends AssignmentComparator {

    public void loadComparators(){
        this.registerComparator(new EditedAssignmentMetaDataTitleFieldComparator());
        this.registerComparator(new EditedAssignmentMetaDataTimeFieldComparator());
        this.registerComparator(new EditedCandidatesComparator());
        this.registerComparator(new NewlyAddedCandidateComparator());
        this.registerComparator(new DeletedCandidatesComparator());
    }

    public void loadValidators(){
        this.registerValidator(new AssignmentIdValidator());
        this.registerValidator(new AssignmentPairMetaDataNullValidator());
    }
}
```

#####Change the output Diff format for time fields
Change the following constants in the AppConstants class to change
the date time format in the output JSON

- DIFF_DATE_FORMAT_STRING : The format of the date/time
- TIME_ZONE_OFFSET : The time zone offset of the output JSON

## Json Schemas

Please use https://www.jsonschema.net to visualize the json schemas.
Load any of the following schemas to view the Json object Structure
Input Json object schema to the Parser should be as follows

```json
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "id",
    "meta",
    "candidates"
  ],
  "properties": {
    "id": {
      "$id": "#/properties/id",
      "type": "integer",
      "title": "The Id Schema",
      "default": 0,
      "examples": [
        1
      ]
    },
    "meta": {
      "$id": "#/properties/meta",
      "type": "object",
      "title": "The Meta Schema",
      "required": [
        "title",
        "startTime",
        "endTime"
      ],
      "properties": {
        "title": {
          "$id": "#/properties/meta/properties/title",
          "type": "string",
          "title": "The Title Schema",
          "default": "",
          "examples": [
            "Title"
          ],
          "pattern": "^(.*)$"
        },
        "startTime": {
          "$id": "#/properties/meta/properties/startTime",
          "type": "string",
          "title": "The Starttime Schema",
          "default": "",
          "examples": [
            "2016-01-20T10:00:00Z"
          ],
          "pattern": "^(.*)$"
        },
        "endTime": {
          "$id": "#/properties/meta/properties/endTime",
          "type": "string",
          "title": "The Endtime Schema",
          "default": "",
          "examples": [
            "2016-01-20T16:00:00Z"
          ],
          "pattern": "^(.*)$"
        }
      }
    },
    "candidates": {
      "$id": "#/properties/candidates",
      "type": "array",
      "title": "The Candidates Schema",
      "items": {
        "$id": "#/properties/candidates/items",
        "type": "object",
        "title": "The Items Schema",
        "required": [
          "id",
          "candidateName",
          "extraTime"
        ],
        "properties": {
          "id": {
            "$id": "#/properties/candidates/items/properties/id",
            "type": "integer",
            "title": "The Id Schema",
            "default": 0,
            "examples": [
              10
            ]
          },
          "candidateName": {
            "$id": "#/properties/candidates/items/properties/candidateName",
            "type": "string",
            "title": "The Candidatename Schema",
            "default": "",
            "examples": [
              "C1"
            ],
            "pattern": "^(.*)$"
          },
          "extraTime": {
            "$id": "#/properties/candidates/items/properties/extraTime",
            "type": "integer",
            "title": "The Extratime Schema",
            "default": 0,
            "examples": [
              0
            ]
          }
        }
      }
    }
  }
}
```

Given Json object pair difference will be in the following Json Schema

```json
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "object",
  "title": "The Root Schema",
  "required": [
    "meta",
    "candidates"
  ],
  "properties": {
    "meta": {
      "$id": "#/properties/meta",
      "type": "array",
      "title": "The Meta Schema",
      "items": {
        "$id": "#/properties/meta/items",
        "type": "object",
        "title": "The Items Schema",
        "required": [
          "field",
          "before",
          "after"
        ],
        "properties": {
          "field": {
            "$id": "#/properties/meta/items/properties/field",
            "type": "string",
            "title": "The Field Schema",
            "default": "",
            "examples": [
              "title"
            ],
            "pattern": "^(.*)$"
          },
          "before": {
            "$id": "#/properties/meta/items/properties/before",
            "type": "string",
            "title": "The Before Schema",
            "default": "",
            "examples": [
              "Title"
            ],
            "pattern": "^(.*)$"
          },
          "after": {
            "$id": "#/properties/meta/items/properties/after",
            "type": "string",
            "title": "The After Schema",
            "default": "",
            "examples": [
              "New Title"
            ],
            "pattern": "^(.*)$"
          }
        }
      }
    },
    "candidates": {
      "$id": "#/properties/candidates",
      "type": "object",
      "title": "The Candidates Schema",
      "required": [
        "edited",
        "added",
        "removed"
      ],
      "properties": {
        "diffCategoryName": {
          "$id": "#/properties/candidates/properties/removed",
          "type": "array",
          "title": "The Removed Schema",
          "items": {
            "$id": "#/properties/candidates/properties/removed/items",
            "type": "object",
            "title": "The Items Schema",
            "required": [
              "id"
            ],
            "properties": {
              "id": {
                "$id": "#/properties/candidates/properties/removed/items/properties/id",
                "type": "integer",
                "title": "The Id Schema",
                "default": 0,
                "examples": [
                  12
                ]
              }
            }
          }
        }
      }
    }
  }
}
```

[Inspera Json Parser Bitbucket]: https://bitbucket.org/ruchindravithanage/insperajsonparser/src/master/ "BitBucket Link"