package parser.comparators;

import parser.domainobjects.Assignment;
import parser.domainobjects.AssignmentDifferenceContainer;
import parser.domainobjects.AssignmentMetaDataFieldDiffIndicator;
import util.AppConstant;
import util.MetaDataFieldName;

import java.time.ZoneOffset;

/**
 * Assignment Object has couple of time fields as Assignment MetaData
 * This comparator will compare "start time" and "end time" attributes in the given pair of Assignments
 */
public class EditedAssignmentMetaDataTimeFieldComparator implements AssignmentAttributesComparatorApi {

    @Override
    public void compare(AssignmentDifferenceContainer assignmentDifferenceContainer,
                        Assignment initialAssignment,
                        Assignment updatedAssignment) {

        //Compare the start time field value from the Assignment Meta Data
        if (initialAssignment.getAssignmentMetaData().getAssignmentStartTime()
                .compareTo(updatedAssignment.getAssignmentMetaData().getAssignmentStartTime()) != 0) {

            assignmentDifferenceContainer.addMetaDataDiff(new AssignmentMetaDataFieldDiffIndicator<>(
                    MetaDataFieldName.ASSIGNMENT_META_DATA_START_TIME_FIELD,
                    initialAssignment.getAssignmentMetaData().getAssignmentStartTime(),
                    updatedAssignment.getAssignmentMetaData().getAssignmentStartTime()));
        }

        //Compare the end time field value from the Assignment Meta Data
        if (initialAssignment.getAssignmentMetaData().getAssignmentEndTime()
                .compareTo(updatedAssignment.getAssignmentMetaData().getAssignmentEndTime()) != 0) {

            assignmentDifferenceContainer.addMetaDataDiff(new AssignmentMetaDataFieldDiffIndicator<>(
                    MetaDataFieldName.ASSIGNMENT_META_DATA_End_TIME_FIELD,
                    initialAssignment.getAssignmentMetaData().getAssignmentEndTime(),
                    updatedAssignment.getAssignmentMetaData().getAssignmentEndTime()));
        }
    }
}
