package util;

import java.time.format.DateTimeFormatter;

/**
 * Parser constants specified per current requirement.
 * following constants can change the structure of the output json
 */
public class AppConstant {

    //The diff indicating output json time format
    public static String DIFF_DATE_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ssX";
    //The diff indicating json will apply this time zone to all the times
    public static int TIME_ZONE_OFFSET = 2;
    public static final DateTimeFormatter DIFF_DATE_FORMATTER = DateTimeFormatter.ofPattern(DIFF_DATE_FORMAT_STRING);

}
